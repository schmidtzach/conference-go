from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# import json

import requests


def get_photo(query):
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    info = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=info)
    content = response.json()
    try:
        lat_coords = content[0]["lat"]
        lon_coords = content[0]["lon"]
    except:
        return None

    info = {
        "lat": lat_coords,
        "lon": lon_coords,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=info)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }


# (KeyError, IndexError)
